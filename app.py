#!/usr/bin/env python

"""Application Signature on pdf - Main script"""

# Internal modules
# import generate_template
import modules.sign_file as sf
import modules.manage_template as mt
import modules.generate_signatures as gs
import modules.manage_files_directories as mfd
import modules.manage_popup as mp

# External modules
import PySimpleGUI as sg
import os
from pathlib import Path

sg.theme('BluePurple')


class Application:
    def __init__(self):
        """Constructor of the main window"""
        main_layout = [
            [sg.Text('Signature on pdf', size=(30, 1), font="bold 14"), sg.Button(key="-INFO-", border_width=0, button_color=(sg.theme_background_color(), sg.theme_background_color()), image_subsample=3, image_data=mfd.convert_file_to_base64("img/info.png"))],
            [sg.Button('Get template')],
            [sg.Button('Generate signatures')],
            [sg.Button('Sign a document')],
            [sg.Button('Quit')]
        ]

        main_window = sg.Window('Signature on pdf', main_layout)

        while True:
            event, values = main_window.read()
            print(event, values)
            if event == sg.WIN_CLOSED or event == 'Quit':
                break
            elif event == "Get template":
                mt.manage_template()
            elif event == "Generate signatures":
                gs.generate_signatures()
            elif event == "Sign a document":
                sf.sign_file()
            elif event == "-INFO-":
                mp.info_popup()

        main_window.close()

        # Deletion of unnecessary files
        mfd.delete_unnecessary_files()


if __name__ == "__main__":
    app = Application()
