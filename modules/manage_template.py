#!/usr/bin/env python

"""Module to manage the template"""

# Internal modules
import modules.manage_files_directories as mfd
import modules.manage_images as mi

# External modules
import PySimpleGUI as sg
import shutil
import fitz
import os

# Local variables
path_template = "template.pdf"
path_template_dimensions = "template-dimensions.pdf"


def manage_template():
    """
    Display the window that manage template.
    """
    manage_template_layout = [
        [sg.Text('What do you want do do ?', size=(30, 1), font="bold 14")],
        [sg.Radio(text="I want to print the template now.", group_id=1, default=True, auto_size_text=True)],
        [sg.Radio(text="I want to download the template to print it later.", group_id=1, auto_size_text=True)],
        [sg.Radio(text="I don't have a printer.", group_id=1, auto_size_text=True)],
        [sg.Button("Validate")],
    ]

    manage_template_window = sg.Window('Manage template', manage_template_layout)

    while True:
        event, values = manage_template_window.read()
        if event == sg.WIN_CLOSED or event == 'Back':
            break
        elif event == 'Validate':
            if values[0]:
                mfd.open_with_default_application(path_template)
            elif values[1]:
                download_template()
            elif values[2]:
                no_printer()


def download_template():
    """
    Copy the template in a directory selected by the user.
    """
    path_saved_template = mfd.open_directory('Select the directory where you want to download the template')
    mfd.copy_file(path_template, os.path.join(path_saved_template, path_template))
    return os.path.join(path_saved_template, path_template)


def no_printer():
    """
    Display the instructions to draw a template on a sheet of paper.
    """
    path = "template-dimensions.png"
    (path, width, height) = mi.resize_image(path, False, 612, 792)
    image_elem = sg.Image(filename=path)

    no_printer_layout = [
        [sg.Text('Draw your own template ?', size=(30, 1), font="bold 14")],
        [sg.Text('A white A4 sheet of paper, a pencil and a ruler are necessary.')],
        [sg.Text('Draw 24 rectangles of dimensions 6,33 cm * 3,45 cm with margins of 1 cm as follow.')],
        [sg.Text('Then, scan the sheet of paper with your smartphone and save it in pdf or image.')],
        [image_elem],
    ]
    no_printer_window = sg.Window('Draw your template', no_printer_layout, finalize=True)
    while True:
        event, values = no_printer_window.read()
        print(event, values)
        if event == sg.WIN_CLOSED:
            break
