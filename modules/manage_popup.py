#!/usr/bin/env python

"""Module to manage popup"""

# External modules
import sys
import PySimpleGUI as sg


def get_text(title, description):
    """
    Prompt user to enter a text and return the entered text
    :return: string, the entered text
    """
    if len(sys.argv) == 1:
        text = sg.popup_get_text(description, title)

        if text is None:
            sg.popup_cancel('Cancelling')
            exit(0)
    else:
        text = sys.argv[0]
    return text


def yes_or_no_popup(title, description):
    """
        Prompt user to select yes or no and return the answer
        :param title: the title of the window
        :param description: the description displays in the window
        :return: string, yes or no
    """
    return sg.popup_yes_no(title, description)


def info_popup():
    """
    Prompt a popup of information about the application
    """
    sg.popup_quick_message("The signature of a document is composed of three principles steps detailed below:",
                           "1.Get the template : The template allows to have several representations of a "
                           "signature in order to not use always the same to sign a document. You should print "
                           "or create the template, sign in each box and scan it.",
                           "2. Generate signatures: Once you have your signed template, you can generate your "
                           "signatures.",
                           "3. Sign a document: Once you have generate your signatures, you can sign pdf "
                           "documents. ",
                           auto_close_duration=22,
                           )
