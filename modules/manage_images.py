#!/usr/bin/env python

"""Module to manage images"""

# External modules
from PIL import Image
import numpy as np
import imageio
import cv2


def resize_image(path_image, initial_resize, width=0, height=0):
    """
    Return a resized image with the new width and height of the image
    :param path_image: the path of the image to resize
    :param initial_resize: a boolean to specify if the first resized
    :param width: the width of the resized image
    :param height: the height of the resized image
    :return: (string, int, int) : (path of the resized image, width of the resized image, height of the resized image)
    """
    img = Image.open(path_image, 'r')
    if initial_resize:
        (resize_width, resize_height) = (img.width // 2, img.height // 2)
    else:
        (resize_width, resize_height) = (width, height)
    img_resized = img.resize((resize_width, resize_height))
    img_resized.save('img_resized.png')
    return 'img_resized.png', resize_width, resize_height


def convert_white_pixels_to_transparent(file_path):
    """
    Convert white pixels to transparent in an image
    :param file_path: the path of the image
    """
    img = Image.open(file_path)

    img = img.convert('RGBA')
    data = img.getdata()

    new_data = []
    for item in data:
        if item[0] == 255 and item[1] == 255 and item[2] == 255:
            new_data.append((255, 255, 255, 0))
        else:
            new_data.append(item)
    img.putdata(new_data)
    img.save(file_path, "PNG")


def convert_green_pixels_to_white(file_path):
    """
    Convert green pixels to white in an image
    :param file_path: the path of the image
    """
    img = Image.open(file_path)

    data = img.getdata()

    new_data = []
    for item in data:
        if item[0] == 0 and item[1] == 255 and item[2] == 0:
            new_data.append((255, 255, 255))
        else:
            new_data.append(item)
    img.putdata(new_data)
    img.save(file_path, "PNG")


def convert_image_to_black_and_white(file_path):
    """
    Convert pixels of the image into white or black pixels
    :param file_path: the path of the file
    """
    # if file_path.split('.')[-1] != "png":
    #     os.rename(file_path, file_path.split('.')[0] + ".png")

    image = imageio.imread(file_path, as_gray=True)

    threshold_value = np.mean(image) - 30
    x_dim, y_dim = image.shape

    for i in range(x_dim):
        for j in range(y_dim):
            if image[i][j] > threshold_value:
                image[i][j] = 255
            else:
                image[i][j] = 0

    imageio.imwrite('signed_template_without_background_color.png', image[:])


def detect_lines_in_image(file_path):
    """
    Detect the lines in the images and colors them in white
    :param file_path: the path of the file
    """
    gray = cv2.imread(file_path)
    edges = cv2.Canny(gray, 50, 150, apertureSize=3)
    cv2.imwrite('edges.jpg', edges)
    min_line_length = 100
    lines = cv2.HoughLinesP(image=edges, rho=1, theta=np.pi / 180, threshold=100, lines=np.array([]),
                            minLineLength=min_line_length, maxLineGap=5)
    a, b, c = lines.shape
    for i in range(a):
        cv2.line(gray, (lines[i][0][0], lines[i][0][1]), (lines[i][0][2], lines[i][0][3]), (255, 255, 255), 3,
                 cv2.LINE_AA)
        cv2.imwrite(file_path, gray)


def detect_rectangles(path):
    """
    Detect the rectangle and return the dimensions of each rectangle
    :param path: the path of the image
    """

    img = cv2.imread(path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ret, thresh = cv2.threshold(gray, 127, 255, 1)
    contours, h = cv2.findContours(thresh, 1, 2)

    dim = []

    for cnt in contours:
        approx = cv2.approxPolyDP(cnt, 0.01 * cv2.arcLength(cnt, True), True)
        if len(approx) == 4:
            (x, y, w, h) = cv2.boundingRect(approx)
            ratio = w / float(h)
            if not 0.95 <= ratio <= 1.05:
                dim.append((x, y, w, h))
                cv2.drawContours(img, [cnt], -1, (0, 255, 0), 8)

    cv2.imwrite("signed_template_without_contours.png", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return dim
