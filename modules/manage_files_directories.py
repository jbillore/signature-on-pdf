#!/usr/bin/env python

"""Module to manage files and directories"""

# External modules
import subprocess
import platform
import webbrowser
import os
import sys
import fitz
import shutil
import PySimpleGUI as sg
import pyscreenshot as ImageGrab
from pathlib import Path
import base64


def open_with_default_application(path):
    """
    Open a file with the default application.
    :param path: : the path of the file
    """
    current_platform = platform.system()
    if current_platform == 'Linux':
        subprocess.call(["xdg-open", path])
    elif current_platform == "Windows":
        os.system("start " + path)
    elif current_platform == "Darwin":
        subprocess.call(["open", path])
    else:
        webbrowser.open_new(path)


def open_file(description, file_types):
    """
    Prompt user to select a file and return the path of the selected file
    :param: string, the description to display in the popup
    :return: string, the path of the selected file
    """
    file_path = ""
    if len(sys.argv) == 1:
        if file_types == 'pdf':
            file_path = sg.popup_get_file(description, "Select PDF file to open",
                                          file_types=(("PDF Files", "*.pdf"),))
        else:
            if file_types == 'pdf_images':
                file_path = sg.popup_get_file(description, "Select PDF file to open",
                                              file_types=(("PDF Files", "*.pdf"), ("Image Files", (
                                                  "*.png", "*.jpg", "*.jpeg", "*.tiff", "*.bmp"))))
        if file_path is None:
            sg.popup_cancel('Cancelling')
            exit(0)
    else:
        file_path = sys.argv[1]
    return file_path


def open_directory(description):
    """
    Prompt user to select a directory and return the path of the selected directory
    :param: description, the description to display in the popup
    :return: string, the path of the selected directory
    """
    if len(sys.argv) == 1:
        dir_path = sg.popup_get_folder(description, "Select directory")
        if dir_path is None:
            sg.popup_cancel('Cancelling')
            exit(0)
    else:
        dir_path = sys.argv[0]
    return dir_path


def get_page(doc, d_list_tab, pno, zoom=0):
    """Return a PNG image for a document page number. If zoom is other than 0, one of the 4 page quadrants are zoomed-in instead and the corresponding clip returned.
        @created: 2018-08-19 18:00:00
        @author: (c) 2018 Jorj X. McKie
        PySimpleGUI/DemoPrograms/Demo_PDF_Viewer.py from https://github.com/PySimpleGUI
    """
    d_list = d_list_tab[pno]  # get display list
    if not d_list:  # create if not yet there
        d_list_tab[pno] = doc[pno].getDisplayList()
        d_list = d_list_tab[pno]
    r = d_list.rect  # page rectangle
    mp = r.tl + (r.br - r.tl) * 0.5  # rect middle point
    mt = r.tl + (r.tr - r.tl) * 0.5  # middle of top edge
    ml = r.tl + (r.bl - r.tl) * 0.5  # middle of left edge
    mr = r.tr + (r.br - r.tr) * 0.5  # middle of right edge
    mb = r.bl + (r.br - r.bl) * 0.5  # middle of bottom edge
    mat = fitz.Matrix(2, 2)  # zoom matrix
    if zoom == 1:  # top-left quadrant
        clip = fitz.Rect(r.tl, mp)
    elif zoom == 4:  # bot-right quadrant
        clip = fitz.Rect(mp, r.br)
    elif zoom == 2:  # top-right
        clip = fitz.Rect(mt, mr)
    elif zoom == 3:  # bot-left
        clip = fitz.Rect(ml, mb)
    if zoom == 0:  # total page
        pix = d_list.getPixmap(alpha=False)
    else:
        pix = d_list.getPixmap(alpha=False, matrix=mat, clip=clip)
    return pix.getPNGData(), pix.height, pix.width


def create_directory(directory_name, parent_directory):
    """
        Create the directory directory_location/directory_name
        :param directory_name: the name of the new directory
        :param parent_directory: the parent directory
        :return: string, the path of the new directory
        """
    directory_path = os.path.join(parent_directory, directory_name)
    try:
        os.mkdir(directory_path)
    except OSError:
        print("Creation of the directory %s failed" % directory_path)
    else:
        print("Successfully created the directory %s" % directory_path)
    return directory_path


def write_in_file(file_path, text):
    """
        Allows to write in a file
        :param file_path: the path of the file
        :param text: the text to write in the file
        """
    file = open(file_path, "a")
    if is_not_empty_file(file_path):
        file.write('\n' + text)
    else:
        file.write(text)
    file.close()


def is_not_empty_file(file_path):
    """
    Allows to know if the file is empty or not
    :param file_path: the path of the file
    :return: boolean, yes if the file is empty, no if not
    """
    return os.path.isfile(file_path) and os.path.getsize(file_path) > 0


def read_lines_file(file_path):
    """
        Allows to read a file line by line
        :param file_path: the path of the file
        :return: list, the lines of the file
        """
    lines = []
    file = open(file_path, "r")
    for line in file:
        lines.append(line.strip('\n'))
    file.close()
    return lines


def select_one_saved_directory(list_saved_directory):
    """
    Allows the user to select a directory among the list of saved directories (directories containing generated signatures)
    :param list_saved_directory: the list of the path of all saved directories
    :return: string, the path of the selected directory
    """
    select_directory_layout = [
        [sg.Text('Select the directory you want to use ?', size=(30, 1), font="bold 14")],
        [sg.Radio(text=path, group_id=1, default=True, auto_size_text=True) for path in list_saved_directory],
        [sg.Button("Validate"), sg.Button("Cancel")],
    ]

    select_directory_window = sg.Window('Select a saved directory', select_directory_layout)

    while True:
        event, values = select_directory_window.read()
        if event == sg.WIN_CLOSED or event == 'Cancel':
            break
        elif event == 'Validate':
            for index, select in values.items():
                if select:
                    select_directory_window.close()
                    return list_saved_directory[index]


def save_element_as_file(element, filename):
    """
    Saves any element as an image file.  Element needs to have an underlying Widget available (almost if not all of them do)
    PySimpleGUI/DemoPrograms/Demo_Graph_Drawing_And_Dragging_Figures_2_Windows.py from https://github.com/PySimpleGUI
    :param element: The element to save (PySimpleGUI element)
    :param filename: The filename to save to. The extension of the filename determines the format (jpg, png, gif, ?)
    """
    widget = element.Widget
    box = (widget.winfo_rootx(), widget.winfo_rooty(), widget.winfo_rootx() + widget.winfo_width(),
           widget.winfo_rooty() + widget.winfo_height())
    grab = ImageGrab.grab(bbox=box)
    grab.save(filename)


def copy_file(initial_file_path, copy_file_path):
    """
    Copy a file in another location
    :param initial_file_path: the path of the initial file
    :param copy_file_path: the path of the copied file
    """
    shutil.copyfile(initial_file_path, copy_file_path)


def delete_file(file_path):
    """
    Delete a file
    :param file_path: the file of the file
    """
    file = Path(file_path)
    if file.is_file():
        os.remove(file_path)


def delete_unnecessary_files():
    """
    Delete unnecessary files
    """
    delete_file("img_resized.png")
    delete_file("template_signed_img.png")
    delete_file('signed_template_without_background_color.png')
    delete_file("signed_template_without_contours.png")
    delete_file('edges.jpg')
    delete_file('img_resized.png')


def convert_file_to_base64(filename):
    """
    Make base64 image from a file.
    PySimpleGUI/DemoPrograms/Demo_Base64_Single_Image_Encoder.py from https://github.com/PySimpleGUI
    :param filename: the name of the file
    :return: the base64 image (in case of success)
    """
    try:
        contents = open(filename, 'rb').read()
        encoded = base64.b64encode(contents)
        return encoded
    except Exception as error:
        sg.popup_error('Cancelled - An error occurred', error)
