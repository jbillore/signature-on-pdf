#!/usr/bin/env python

"""Module to generate the signatures"""

# Internal modules
import modules.manage_files_directories as mfd
import modules.manage_popup as mpp
import modules.manage_images as mi

# External modules
import fitz
from PIL import Image
import os


def generate_signatures():
    """
    Generates the 24 images of signature in a new directory (name and location selected by the user)
    """
    # Selection of the signed template
    template_signed_path = mfd.open_file("Select the signed template", "pdf_images")

    # Creation of the directory that will contain the signatures (name and location selected by user)
    signatures_directory_name = mpp.get_text("Directory name", "Indicate the name of the directory that will contain "
                                                               "the signatures")
    signatures_directory_location = mfd.open_directory("Select the location of the directory that will "
                                                       "contain the signatures")
    signatures_directory_path = mfd.create_directory(signatures_directory_name, signatures_directory_location)

    # Generation of the 24 images of signature
    generate_images_signature(template_signed_path, signatures_directory_path)

    # Allow to chose to save or not the path of the directory containing the signatures
    save_signatures_directory_path = mpp.yes_or_no_popup("Save the path of the directory",
                                                         "Do you want to save the path of the directory "
                                                         "containing the signatures ?")
    if save_signatures_directory_path == "Yes":
        mfd.write_in_file("save_signatures_directory_path.txt", signatures_directory_path)

    return save_signatures_directory_path


def generate_images_signature(path, signatures_directory_path):
    """
    Function to generate the 24 images of signature
    :param path: the path of the signed template
    :param signatures_directory_path: the path of the directory that will contain the images of signature
    """
    path_template = 'signed_template_without_background_color.png'
    is_image = False
    if path.split('.')[-1] == "pdf":
        template_signed = fitz.Document(path)
        if len(template_signed) == 1:
            template_signed_img = template_signed[0].getDisplayList().getPixmap(alpha=False)
            path_img = "template_signed_img.png"
            template_signed_img.writePNG(path_img)
            mi.convert_image_to_black_and_white(path_img)
    else:
        is_image = True
        if path.split('.')[-1] != "png":
            new_path = path.split('.')[0] + ".png"
            os.rename(path, new_path)
            mi.convert_image_to_black_and_white(new_path)
        else:
            mi.convert_image_to_black_and_white(path)

    mi.convert_white_pixels_to_transparent(path_template)
    rect_dims = mi.detect_rectangles(path_template)
    path_template = "signed_template_without_contours.png"
    mi.convert_green_pixels_to_white(path_template)
    mi.convert_white_pixels_to_transparent(path_template)
    cutting_template(path_template, signatures_directory_path, is_image, rect_dims)


def cutting_template(path_template, signatures_directory_path, is_image, rect_dims):
    """
    Cut the template in several images.
    :param: path_template: the path of the template
    :param: signatures_directory_path: the path of the directory that will contain the images of signature
    :param: is_image: boolean that indicates if the template is a image or not (i.e. a pdf)
    :param: rect_dim: the dimensions of all the rectangles contained in the template
    """
    img = Image.open(path_template)
    i = 1

    w = img.width
    h = img.height

    for dimension in rect_dims:
        if not is_image:
            if ((w - 60) / 3) - 20 <= dimension[2] <= ((w - 60) / 3) + 20 and (h / 8) - 20 <= dimension[3] <= (
                    h / 8) + 20:
                img.crop((dimension[0], dimension[1], dimension[0] + dimension[2], dimension[1] + dimension[3])).save(
                    signatures_directory_path + os.sep + 'signature%i.png' % i, "PNG")
        else:
            img.crop((dimension[0], dimension[1], dimension[0] + dimension[2], dimension[1] + dimension[3])).save(
                signatures_directory_path + os.sep + 'signature%i.png' % i, "PNG")
        i += 1
