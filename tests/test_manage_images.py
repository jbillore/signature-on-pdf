#!/usr/bin/env python

"""Tests of the modules manage_image.py"""

# Internal modules
import modules.manage_images as mi

# External modules
from PIL import Image


def test_resize_image_initial_resize():
    """
    Test of resize_image when initial resize is True
    """
    path_image = "tests/resources/signature1-1.png"
    img_before = Image.open(path_image, 'r')
    (path_image, width, height) = mi.resize_image(path_image, True)
    img_after = Image.open(path_image, 'r')
    assert (img_after.width, img_after.height) == (img_before.width // 2, img_before.height // 2)


def test_resize_image_not_initial_resize():
    """
    Test of resize_image when the initial resize is False
    """
    path_image = "tests/resources/signature1-1.png"
    (path_image, width, height) = mi.resize_image(path_image, False, 20, 20)
    img_after = Image.open(path_image, 'r')
    assert (img_after.width, img_after.height) == (20, 20)
