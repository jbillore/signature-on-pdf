#!/usr/bin/env python

"""Tests of the modules manage_template.py"""

# Internal modules
import modules.manage_template as mt

# External modules
import os


def test_download_template():
    """
    Test of download_template
    """
    path = mt.download_template()
    correct_path = "tests/created_during_tests/template.pdf"
    obtained_path = path.split('/')[-3] + '/' + path.split('/')[-2] + '/' + path.split('/')[-1]
    assert obtained_path == correct_path
    assert os.path.exists(correct_path)

