# Tests scenario

## Dependency

`pytest` need to be installed.

`pip install -U pytest`

___

## Architecture

* **Files`test_*`:** tests files of the modules.
* **resources:** directory that contains content needed to run the tests.
* **creating_during_tests:** directory that contains content generated during tests.
* **delete_unnecessary_contents.py:** allows to delete all the content of the directory `creating_during_tests`. 
* **demonstration:** directory that contains demonstration video that explains how to run the tests.
___

## Run the tests

To run tests, execute the following command in the source directory of the project : `python3 -m pytest `

Different windows will appear in the screen and you should realise actions to pass the tests. Each action is detailed below in the chronological order.

1.  **Window :** Select PDF file to open
    * Click on the `Browse` button.
    * Go to the `tests` directory and then the  `resources` directory.
    * Select the file `template_signe.pdf`.
    * Click on the `Ok` button.

2. **Window :** Directory name
    * Enter `signatures_directory` in the text field.
    * Click on the `Ok` button.
    
3. **Window :** Select directory
    * Click on the `Browse` button.
    * Go to the `tests` directory.
    * Select the `created_during_tests` directory (you need to double click on it to selected it).
    * Click on the `Ok` button.

4. **Window :** Save the path of the directory
    * Click on the `No` button.
    
5. **Window :** Select directory (actions to realise are writen in the window)
    * Click on the `Browse` button.
    * Go to the `tests` directory.
    * Select the `resources` directory.
    * Click on the `Ok` button.
    
6. **Window :** Select PDF file to open (actions to realise are writen in the window)
    * Click on the `Browse` button.
    * Go to the `tests` directory and then the  `resources` directory.
    * Select the file `template_signe.pdf`.
    * Click on the `Ok` button.
    
7. **Window :** Select PDF file to open (actions to realise are writen in the window)
    * Click on the `Browse` button.
    * Go to the `tests` directory and then the `resources` 
    * In `Type of file`, select `Image Files`.
    * Select the file `template_signe_img.png`.
    * Click on the `Ok` button.

8. **Window :** Select a saved directory
    * Select `dir_2`.
    * Click on the `Validate` button.
    
9. **Window :** Text (actions to realise are writen in the window)
    * Write `text` in the text field.
    * Click on the `Ok` button.
    
10. **Window :** Test (actions to realise are writen in the window)
    * Click on the `Yes` button.
    
11. **Window :** Test (actions to realise are writen in the window)
    * Click on the `No` button.  
    
12. **Window :**  Select directory
    * Click on the `Browse` button.
    * Go to the `tests` directory and then the  `created_during_tests` directory.
    * Click on the `Ok` button.

13. **Window :** Select PDF file to open
    * Click on the `Browse` button.
    * Go to the `tests` directory and then the  `resources` directory.  
    * Select the file `document.pdf`.
    * Click on the `Ok` button.

14. **Window :** Used the saved directory
    * Click on the `No` button.

15. **Window :** Select directory
    * Click on the `Browse` button.
    * Go to the `tests` directory and then the  `resources` directory.
    * Select the `signatures_directory` directory.
    * Click on the `Ok` button.

16. **Window :** Signature
    * Close the window.
    
17. **Window :** Used the saved directory
    * Click on the `No` button.
    
18. **Window :** Select directory
    * Click on the `Browse` button.
    * Go to the `tests` directory and then the  `resources` directory.
    * Select the `signatures_directory` directory.
    * Click on the `Ok` button.
    
19. **Window :** Save as
    * Click on the `Browse` button.
    * Go to the `tests` directory and then the  `created_during_tests` directory.
    * Indicate `signed_doc.pdf` as the name of the file.
    * Click on the `Ok` button.
    
At the end of the execution of the tests, run the following command to delete the files and directories created during tests : `python3 tests/delete_unnecessary_contents.py 
`

The demonstration video below explains how to run the tests.

![Demonstration](demonstration/demo-tests-SignatureOnPdf.mp4)