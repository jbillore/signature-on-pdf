#!/usr/bin/env python

"""Allows to delete all unnecessary content"""

import os
import shutil

if __name__ == "__main__":
    dir = "tests/created_during_tests"
    shutil.rmtree(dir)
    os.mkdir("tests/created_during_tests")
    fichier = open("tests/created_during_tests/.gitkeep", "w")
    fichier.close()
