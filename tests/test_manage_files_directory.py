#!/usr/bin/env python

"""Tests of the modules manage_files_directories.py"""

# Internal modules
import modules.manage_files_directories as mfd

# External modules
import os
import PySimpleGUI as sg
import shutil


def test_create_directory():
    """
    Test of create_directory
    """
    directory_name = "directory"
    parent_directory = "tests/created_during_tests/"
    directory_path = mfd.create_directory(directory_name, parent_directory)
    assert directory_path == "tests/created_during_tests/directory"
    assert os.path.exists(directory_path)


def test_write_in_file():
    """
    Test of write_in_file
    """
    file_path = "tests/resources/file.txt"
    file = open(file_path, "r")
    file_lines = file.readlines()
    file.close()
    nb_lines_before = len(file_lines)
    mfd.write_in_file(file_path, "Some content")
    file = open(file_path, "r")
    file_lines = file.readlines()
    file.close()
    nb_lines_after = len(file_lines)
    last_line = file_lines[nb_lines_after - 1]
    assert nb_lines_after == nb_lines_before + 1
    assert last_line == "Some content"


def test_is_not_empty_file_not_empty():
    """
    Test of is_not_empty_file with a not empty file
    """
    assert mfd.is_not_empty_file("tests/resources/file.txt")


def test_is_not_empty_file_empty():
    """
    Test of is_not_empty_file with an empty file
    """
    assert not mfd.is_not_empty_file("tests/resources/empty_file.txt")


def test_is_not_empty_file_not_exists():
    """
    Test of is_not_empty_file with a file that does not exist
    """
    assert not mfd.is_not_empty_file("tests/resources/file_not_exist.txt")


def test_read_lines():
    """
    Test of read_lines
    """
    lines = [
        "This is the first line.",
        "This is the second line.",
        "This is the third line."
    ]
    assert mfd.read_lines_file("tests/resources/read_file.txt") == lines


def test_open_directory():
    """
    Test of open_directory
    """
    dir_path = "tests/resources"
    description = "Test - Select the directory resources in the tests directory"
    path = mfd.open_directory(description)
    obtained_path = path.split('/')[-2] + '/' + path.split('/')[-1]
    assert obtained_path == dir_path


def test_open_file_pdf_file():
    """
    Test of open_file with a pdf file
    """
    file_path = "tests/resources/template_signe.pdf"
    description = "Test - select the file template_signe.pdf in the resources directory"
    path = mfd.open_file(description, 'pdf')
    obtained_path = path.split('/')[-3] + '/' + path.split('/')[-2] + '/' + path.split('/')[-1]
    assert obtained_path == file_path


def test_copy_file():
    """
    Test of copy_file
    """
    copy_file_path = "tests/created_during_tests/template_signe.pdf"
    mfd.copy_file("tests/resources/template_signe.pdf", copy_file_path)
    assert os.path.exists(copy_file_path)


def test_open_file_image_file():
    """
    Test of open_file with a image file
    """
    file_path = "resources/template_signe_img.png"
    description = "Test - select the file template_signe_img.png in the resources directory"
    path = mfd.open_file(description, 'pdf_images')
    obtained_path = path.split('/')[-2] + '/' + path.split('/')[-1]
    assert obtained_path == file_path


def test_save_element_as_file():
    """
    Test of save_element_as_file
    """
    file_path = "tests/created_during_tests/save_element_as_file.pdf"
    layout = [
        [sg.Text('This is a test', key="-TEXT-")]
    ]
    window = sg.Window(title="title", layout=layout, finalize=True)
    element = window["-TEXT-"]
    mfd.save_element_as_file(element, file_path)
    assert os.path.exists(file_path)


def test_select_one_saved_directory():
    """
    Test of select_one_saved_directory
    """
    list_saved_directory = ["dir_1", "dir_2", "dir_3"]
    selected_diredtory = mfd.select_one_saved_directory(list_saved_directory)
    assert selected_diredtory == list_saved_directory[1]


def test_delete_file():
    """
    Test of delete_file
    """
    file_path = "save_element_as_file.pdf"
    mfd.delete_file(file_path)
    assert not os.path.exists(file_path)


def test_delete_unnecessary_files():
    """
    Test delete_unnecessary_files
    """
    mfd.delete_unnecessary_files()
    assert not os.path.exists("img_resized.png")
    assert not os.path.exists("template_signed_img.png")
    assert not os.path.exists('signed_template_without_background_color.png')
    assert not os.path.exists('edges.jpg')
    assert not os.path.exists('img_resized.png')

