#!/usr/bin/env python

"""Tests of the modules manage_pop_uo.py"""

# Internal modules
import modules.manage_popup as mp


def test_get_text():
    """
     Test of get text
    """
    assert mp.get_text("Text", "Write : text") == "text"


def test_yes_or_no_popup_yes():
    """
    Test of yes_or_no_popup
    """
    assert mp.yes_or_no_popup("Test", "Click Yes") == "Yes"


def test_yes_or_no_popup_no():
    """
    Test of yes_or_no_popup
    """
    assert mp.yes_or_no_popup("Test", "Click No") == "No"
