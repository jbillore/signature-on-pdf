#!/usr/bin/env python

"""Tests of the modules generate_signatures.py"""

# Internal modules
import modules.generate_signatures as gs

# External modules
import os

file = open("save_signatures_directory_path.txt", "r")
file_lines = file.readlines()
file.close()
nb_lines_before = len(file_lines)
signatures_directory_path = "tests/created_during_tests/signatures_directory/"
signature_saved = gs.generate_signatures()


def test_generate_signatures_create_directory():
    """
    Test of generate_signatures - verification of the creation of the directory
    """
    assert os.path.exists(signatures_directory_path)


def test_generate_signatures_create_24_files():
    """
    Test of generate_signatures - verification of the generation of 24 files.
    """
    assert len(os.listdir(signatures_directory_path)) == 24


def test_generate_signatures_create_24_images():
    """
    Test of generate_signatures - verification of the generation of images.
    """
    list_dir = os.listdir(signatures_directory_path)
    for element in list_dir:
        assert element.split('.')[-1] == "png"


def test_generate_signatures_saved_directory():
    """
    Test of generate_signatures - verification of the saving (or not) of the path of the directory
    """
    if signature_saved == "Yes":
        file = open("save_signatures_directory_path.txt", "r")
        file_lines = file.readlines()
        file.close()
        nb_lines_after = len(file_lines)
        assert nb_lines_after == nb_lines_before + 1
    else:
        file = open("save_signatures_directory_path.txt", "r")
        file_lines = file.readlines()
        file.close()
        nb_lines_after = len(file_lines)
        assert nb_lines_after == nb_lines_before
