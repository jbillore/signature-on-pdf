#!/usr/bin/env python

"""Tests of the modules sign_file.py"""

# Internal modules
import modules.sign_file as sf

# External modules
import os
import fitz
import PySimpleGUI as sg


def test_sign_file():
    """
    Test of sign_file
    """
    file_path, dir_path = sf.sign_file()
    correct_sign_file_path = "tests/resources/document.pdf"
    correct_signatures_dir_path = "tests/resources/signatures_directory"
    sign_file_path = file_path.split('/')[-3] + '/' + file_path.split('/')[-2] + '/' + file_path.split('/')[-1]
    signatures_dir_path = dir_path.split('/')[-3] + '/' + dir_path.split('/')[-2] + '/' + dir_path.split('/')[-1]
    assert sign_file_path == correct_sign_file_path
    assert signatures_dir_path == correct_signatures_dir_path


def test_selection_signatures_directory_not_selected_saved_directory():
    """
    Test of selection_signatures_directory in case of not selected saved directories
    """
    correct_signatures_dir_path = "tests/resources/signatures_directory"
    dir_path = sf.selection_signatures_directory()
    signatures_dir_path = dir_path.split('/')[-3] + '/' + dir_path.split('/')[-2] + '/' + dir_path.split('/')[-1]
    assert signatures_dir_path == correct_signatures_dir_path


def test_select_signature():
    """
    Test of select_signature
    :return:
    """
    signatures_dir_path = "tests/resources/signatures_directory"
    signature_path = sf.select_signature(signatures_dir_path)
    assert os.path.exists(signature_path)
    assert signature_path.split('.')[-1] == 'png'
    assert signature_path.split('/')[-1] in os.listdir(signatures_dir_path)


def test_save_signed_file():
    """
    Test of save_signed_file
    """
    sign_doc = "tests/created_during_tests/signed_doc.pdf"
    doc_path = "tests/resources/document.pdf"
    doc = fitz.Document(doc_path)
    path = sf.save_signed_file(3, doc, "tests/resources/signature1-1.png", ((10, 500), (109, 447)), 792)
    obtained_path = path.split('/')[-3] + '/' + path.split('/')[-2] + '/' + path.split('/')[-1]
    signed_doc = fitz.Document(obtained_path)
    assert os.path.exists(obtained_path)
    assert obtained_path == sign_doc
    assert len(signed_doc) == len(doc)

