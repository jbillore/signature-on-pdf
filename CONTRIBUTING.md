# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email or any other method with the author of this project (@jbillore).

Some examples of contributions:
* Reporting a bug
* Discussing the current state of the code
* Submitting a fix
* Proposing new feature
* Becoming a maintainer

Please follow these steps to contribute to this project:
1. Create a new issue that describe the contribution you want to make (improvement, bug, new feature, etc.). Do not hesitate to detail your description and add images, videos.
2. Add one of these label : Bug, Feature or Improvement.
3. Assign the issue to @jbillore in order to I can view and validate it.

You can also participate to the development of the project. To do so, follow these steps:
1. In the board `Development monitoring`, choose an opened issue in the list `ToDev`, in which nobody is already assigned, and assign the issue to you.
2. Once you decided to start the development of the issue, follow these steps:
    * Delete the label `ToDev` and add `DevInProcess`. 
    * Create a new branch from to `dev`. The name of the branch would follow the following syntax : `type_of_dev/descriptionContributing` (`type_of_dev` corresponds to the label of the issue : bug, feature or improvements).
        * _Example:_ `ìmprovement/addLoadingBar` 
    * When you commit code, follow these syntax:
        * For bug: `Fix(#issue_number): description`. 
        * For feature: `Feature(#issue_number): description`.
        * For improvement: `Improve(#issue_number): description`.
3. Once the development is completed, follow these steps:
    * Delete the label `DevInProcess` and add `ToValidate`. 
    * Create a merge request with a description of the changes made.
    * Assign the pull request to @jbillore.
4. If your issue need specification, add the label `ToSpecify`. 
5. If your issue need to be test, add the label `ToTest`.
