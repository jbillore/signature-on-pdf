# PAO - Signature on pdf

## Table of Contents

1. [Overview](#overview)
2. [Installing the project](#installing-the-project)
    1. [Dependencies](#dependencies)
3. [Running the project](#running-the-project)
4. [Features](#features)
5. [Architecture of the project - For developers](#architecture-of-the-project---for-developers)
6. [Ideas of improvement](#ideas-of-improvement)
7. [Contributing](#contributing)
8. [Acknowledgements](#acknowledgements)
9. [License](#license)
    

## Overview 

Sign pdf numerically more easily and in a more authentic way thanks to _Signature on pdf_. With this graphical application developed in Python, you can use your handwritten signature to sign pdf document, choose the size and the location of the signature in the document. _Signature on pdf_ is available on Linux, Windows and MacOs.

This project has been developed by Jessica Billoré, 5th year student at INSA Rouen Normandie (France) in the speciality Informatics and information technology (ITI - Informatique et Technologie de l'Information) as part of a Deepening and Opening Project (PAO - Projet d'Aprofondissement et d'Ouverture) coordinated by Sebastien Bonnegent.

 
## Installing the project

1. Download or clone the project.
   * To clone it: `git clone https://gitlab.insa-rouen.fr/jbillore/pao-signature-sur-pdf.git`

2. You need to have `python3` installed in your OS. 
    * [Download Python3](https://www.python.org/downloads/)


### Dependencies

To run the project, you need to install the following packages:
1. **[PySimpleGUI](https://pypi.org/project/PySimpleGUI/):** `pip install pysimplegui`
2. **[pathlib](https://pypi.org/project/pathlib/):** `pip install pathlib`
3. **[fitz](https://pypi.org/project/fitz/) ([PyMuPDF](https://pypi.org/project/PyMuPDF/)) :** `pip install fitz` or `pip install pymupdf`
4. **[PIL](https://pillow.readthedocs.io/en/stable/installation.html):** `pip install --upgrade Pillow`
5. **[pyscreenshot](https://pypi.org/project/pyscreenshot/):** `pip install pyscreenshot`
6. **[cv2](https://pypi.org/project/opencv-python/):** `pip install opencv-python`
7. **[imageio](https://pypi.org/project/imageio/):** `pip install imageio`
8. **[numpy](https://pypi.org/project/numpy/) :** `pip install numpy`



## Running the project

1. Open a console in the project directory.
2. Execute `python3 app.py`


## Features

The signature of a document is composed of three principles steps detailed below:

![menu](img/menu.png)


1. **Get the template :** The template allows to have several representations of a signature in order to not use always the same to sign a document. The template is the file `template.pdf`. To get the template, click on the button _Get the template_ on the main window. Three options are available to obtain the template :
    
    ![manage-template](img/manage-template.png)
    
    1. _Open and print the template:_ It is the first option. The template will be open with your favorite pdf reader application and you can print it directly.
    2. _Download the template to print it later:_ It is the second option. You can choose the location you want to download the template to open and print it later.
    3. _Create your own template:_ It is the last option. If you do not have a printer, you can create your own template thanks to the instructions displayed.
    
    One you have the template, you should sign in each box. Then, scan the signed template and save it in your computer as pdf or image. You can also scan the document with your smartphone but the quality and luminosity of the picture need to be good.
    You can also sign the template numerically but your signatures wil be less authentic.
    
2. **Generate signatures:** Once you have your signed template, you can generate your signatures. To do so, click on the button _Generate signatures_ in the main window. A window will allow you to select the signed template (pdf or image format). 24 signatures in image format will be generated thanks to the signed template. Two windows will allows you to indicate the name and the location of the directory that will contain the 24 signatures. 

    The generation of the signatures can be a bit slow if you use a signed template in image format. Once the generation finished, you should go to the directory containing the signatures and delete the generated signatures which do not suit you.

    When the generate of the signatures is completed, you can chose to save the location of this directory or not. If you save the location, you will not need to select the directory manually each time you want to sign a document. You can save several directories, for instance if you want to use the application for different persons.
    The file `save_signatures_directory_path.txt` contains the path of the saved directories.
    
3. **Sign a document:** Once you have generate your signatures, you can sign pdf documents. To do so, click on the button `Sign a document` in the main window. The first step is to select the document you want to sign. Then, you need to select the directory that contains your signatures. If you have one or several saved directories, you can choose which one to use.
    After that, a window with the pdf document and your signature that will appears. For more authenticity, the signature is selected randomly among your 24 signatures. 
    
    ![sign-file](img/Sign-pdf.png)
    
    To sign your document, you can :
    * Select the page on which you want to sign thanks to the buttons `Prev` and `Next`. You can also directly access to the page you want.
    * Click on `On` next to `Move the signature` to move the signature where you want in the document.
    * Resize the signature thanks to the buttons `+` and `-`.
    * Click on the button `Save` to save your document. You can select the name and the location of the signed document.  

These three steps are summarized and displayed when you click in the `i` button at the right corner of the main window.

The demonstration below details these three steps:

![Demonstration](demonstrations/demo-App-SignatureOnPdf.mp4)

## Architecture of the project - For developers

The project is composed of the following files and directories :

* _app.py:_ it is the main application, the file to run in order to use the application.
* _save_signatures_directory_path.txt:_ the file that contains the path of the saved directories. These directories contains signatures generated by the application. 
* _template.pdf:_ the template the user need to sign manually.
* _template-dimensions.pdf:_ the template with dimensions information to allow the user to create it himself.
* _modules:_ directory that contains all the internal modules needed to run the application.
* _tests:_ directory that contains the files needed to test the application. The explanations about how to run the tests are explained in the file `tests_scenario.md`, read it before run the tests. A demonstration video that explains how to run the tests is also available.
* _demonstrations:_ directory that contains a demonstration video that explains how to use the application.
* _img:_ directory that contains images needed in this ReadMe.


## Ideas of improvement

This is a list of ideas of improvement for the application that I do not have time to implement but that I think could be useful.

- [ ] (#1) - Possibility to edit, modify and delete the saved directories that contains the signatures.
- [ ] (#2) - Possibility to add the initials of the person in footer or in a specific location on each page of the document.
- [ ] (#3) - Possibility to change the language of the application (for instance English or French).
- [ ] (#4) - Possibility to change the theme of the application (the theme is managed by this line in the `app.py` file : `sg.theme('BluePurple')`. [See themes available](https://raw.githubusercontent.com/PySimpleGUI/PySimpleGUI/master/images/for_readme/ThemePreview.jpg)
- [ ] (#5) - Add a loading bar during the generation of the signatures
- [ ] (#6) - Improve the generation of the signature with image template
- [ ] (#8) - Add an identity to the application
- [ ] (#9) - Being able to generate signatures in a directory tree structure.

## Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email or any other method with the author of this project (@jbillore).

Some examples of contributions:
* Reporting a bug
* Discussing the current state of the code
* Submitting a fix
* Proposing new feature
* Becoming a maintainer

Please follow these steps to contribute to this project:
1. Create a new issue that describe the contribution you want to make (improvement, bug, new feature, etc.). Do not hesitate to detail your description and add images, videos.
2. Add one of these label : Bug, Feature or Improvement.
3. Assign the issue to @jbillore in order to I can view and validate it.

You can also participate to the development of the project. To do so, follow these steps:
1. In the board `Development monitoring`, choose an opened issue in the list `ToDev`, in which nobody is already assigned, and assign the issue to you.
2. Once you decided to start the development of the issue, follow these steps:
    * Delete the label `ToDev` and add `DevInProcess`. 
    * Create a new branch from to `dev`. The name of the branch would follow the following syntax : `type_of_dev/descriptionContributing` (`type_of_dev` corresponds to the label of the issue : bug, feature or improvements).
        * _Example:_ `ìmprovement/addLoadingBar` 
    * When you commit code, follow these syntax:
        * For bug: `Fix(#issue_number): description`. 
        * For feature: `Feature(#issue_number): description`.
        * For improvement: `Improve(#issue_number): description`.
3. Once the development is completed, follow these steps:
    * Delete the label `DevInProcess` and add `ToValidate`. 
    * Create a merge request with a description of the changes made.
    * Assign the pull request to @jbillore.
4. If your issue need specification, add the label `ToSpecify`. 
5. If your issue need to be test, add the label `ToTest`.
 


## Acknowledgements

This project uses the two following GitHub projects :

* [PySimpleGUI](https://github.com/PySimpleGUI)
* [PyMuPDF](https://github.com/pymupdf/PyMuPDF)

I would like to acknowledge the contributors of these projects for them work that has been very useful in the development of my project. Do not hesitate to take a look at it.

I also would like acknowledge the author of the project that has inspired my work : [Falsisign](https://gitlab.com/edouardklein/falsisign)

Finally, I would like to acknowledge Sebastien Bonnegent who has coordinated the project and guide me in my work.

## License

This project is an open source project under GNU Public License, version 3 (GPL 3). Copie of the license are included in the file [LICENSE](LICENSE).
